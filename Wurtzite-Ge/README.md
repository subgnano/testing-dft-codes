# Hexagonal Ge (Wurtzite) band structure

Status of each code:

- **VASP:** MGGA+SOC gives great results!
- **QE:** MGGA works only without spinors.
- **Octopus:** testing again with branch *noncollinear_becke_roussel*
- **GPAW:** MGGA works only without spinors.
- **Exciting:** didn't try yet

## VASP: PBE vs MGGA + SOC

![Hex-Ge VASP PBE vs MGGA](VASP-MGGA/VASP-MGGA.png "Hex-Ge VASP PBE vs MGGA"){width=500}

