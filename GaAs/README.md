# GaAs band structure

Status of each code:

- **VASP:** MGGA+SOC gives great results!
- **QE:** MGGA works only without spinors.
- **Octopus:** testing branch *noncollinear_becke_roussel*
    - version: `octopus mercatoris (git commit af291caee72a5012d21113c5f1649529e0a9d931)`
    - LDA no SOC: works fine
    - LDA + SOC: severe limitations
        - Full relativistic ZORA fails. Use `RelativisticCorrection = spin_orbit`
        - Spin-orbit coupling and k-point symmetries not implemented. Use `KPointsUseSymmetries = no`
        - Much slower than *LDA no SOC*, possibly due to `KPointsUseSymmetries = no`
    - GGA with spinors not implemented!
    - MGGA: testing with `XCFunctional = mgga_x_tb09 + lda_c_pz_mod` (is it the same as VASP's MBJ?)
        - Require `FilterPotentials = filter_none`
        - Without SOC: works great!
        - With SOC: **FATAL ERROR: MGGA with spinors not implemented**
- **GPAW:** MGGA works only without spinors.
    - PBE+SOC not converging
    - MGGA without SOC: gap too small using **TB09** or **SCAN**
- **Exciting:** didn't try yet

## VASP: PBE vs MGGA + SOC

![GaAs VASP PBE vs MGGA](VASP-MGGA/VASP-MGGA.png "GaAs VASP PBE vs MGGA"){width=500}

## Octpus: LDA vs MGGA

![Octpus](Octopus-SOC/Octopus.png "LDA with and without SOC"){width=500}

