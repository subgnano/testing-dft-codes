#!/bin/bash

# to allow for selective rm command with !(...) notation
shopt -s extglob

# PATH to VASP bins
VASP="$HOME/vasp/vasp.5.4.4-2017/bin/"

# SCF calculation
echo "SCF calculation"
cd SCF
rm -rf -v !(INCAR|KPOINTS|POSCAR|POTCAR)
$VASP/vasp_ncl &> out.log
cd ..

# BANDS calculation
echo "BANDS calculation"
cd BANDS
rm -rf -v !(INCAR|KPOINTS)
cp ../SCF/POSCAR .
cp ../SCF/POTCAR .
cp ../SCF/CHGCAR .
cp ../SCF/WAVECAR .
$VASP/vasp_ncl &> out.log
cd ..

# VASPKIT: plot bands
echo "VASPKIT 211: plot bands along k-path"
cd BANDS
source ~/.miniconda3/etc/profile.d/conda.sh
conda activate $HOME/testing-dft-codes/.conda/
vaspkit -task 211
cd ..


