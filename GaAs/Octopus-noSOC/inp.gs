FromScratch = true
CalculationMode = gs
PeriodicDimensions = 3
Spacing = 0.5

%LatticeVectors
  0.0 | 0.5 | 0.5 
  0.5 | 0.0 | 0.5
  0.5 | 0.5 | 0.0
%

a = 5.65325*angstrom
%LatticeParameters
 a | a | a
%

%ReducedCoordinates
 "Ga" | 0.0 | 0.0 | 0.0 
 "As" | 1/4 | 1/4 | 1/4 
%

nk = 4
%KPointsGrid
  nk |  nk |  nk
 0.5 | 0.5 | 0.5
 0.5 | 0.0 | 0.0
 0.0 | 0.5 | 0.0
 0.0 | 0.0 | 0.5
%

%Output
  dos
%

KPointsUseSymmetries = yes
KPointsUseTimeReversal = no
ExperimentalFeatures = yes

PseudopotentialSet = hgh_lda

ExtraStates = 6
ExtraStatesToConverge = 3


