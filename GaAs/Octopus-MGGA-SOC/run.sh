#!/bin/bash

# to allow for selective rm command with !(...) notation
shopt -s extglob
# clean
rm -rf -v !(bands.ipynb|inp|run.sh)

octopus_path="$HOME/bin/octopus/bin/"
echo "Running from $octopus_path"
$octopus_path/octopus > gs.log 2>&1 
