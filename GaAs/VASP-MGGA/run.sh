#!/bin/bash

# to allow for selective rm command with !(...) notation
shopt -s extglob

# PATH to VASP bins
VASP="$HOME/vasp/vasp.5.4.4-2017/bin/"

# PBE calculation
echo "PBE SCF calculation"
cd PBE
rm -rf -v !(INCAR|POSCAR|POTCAR|KPATH.in)
vaspkit -task 251 -kps M -kpr 0.04 -kprb 0.04 # generate kpoints
$VASP/vasp_ncl &> out.log
cd ..

# VASPKIT: plot bands
echo "VASPKIT 252: plot bands"
cd PBE
source ~/.miniconda3/etc/profile.d/conda.sh
conda activate $HOME/testing-dft-codes/.conda/
vaspkit -task 252
cd ..

# MGGA SCF calculation
echo "MGGA SCF calculation"
cd MGGA
rm -rf -v !(INCAR)
cp ../PBE/POSCAR .
cp ../PBE/POTCAR .
cp ../PBE/KPATH.in .
cp ../PBE/KPOINTS .
cp ../PBE/WAVECAR .
cp ../PBE/CHGCAR .
$VASP/vasp_ncl &> out.log
cd ..

# VASPKIT: plot bands
echo "VASPKIT 252: plot bands"
cd MGGA
source ~/.miniconda3/etc/profile.d/conda.sh
conda activate $HOME/testing-dft-codes/.conda/
vaspkit -task 252
cd ..


