#!/bin/bash

# to allow for selective rm command with !(...) notation
shopt -s extglob
# clean
rm -rf -v !(bands.ipynb|inp.bands|inp.gs|run.sh)

octopus_path="$HOME/bin/octopus/bin/"
echo "Running from $octopus_path"

ln -fs inp.gs    inp
$octopus_path/octopus > gs.log 2>&1 

ln -fs inp.bands inp
$octopus_path/octopus > bands.log 2>&1 

rm -f inp
