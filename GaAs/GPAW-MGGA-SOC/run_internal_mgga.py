from ase.build import bulk
from gpaw import GPAW, PW, FermiDirac
import matplotlib.pyplot as plt
import numpy as np

# Perform standard ground state calculation (with plane wave basis)
si = bulk('GaAs', 'zincblende', 5.6531)
calc = GPAW(mode=PW(400),
            # gpaw-24.6.0
            #####################################
            # MGGA_X_SCAN+MGGA_C_SCAN, TB09, and GLLBSC:
            # python: c/xc/libxc.c:641: NewlxcXCFunctionalObject: Assertion `nspin==XC_UNPOLARIZED || nspin==XC_POLARIZED' failed.
            #####################################
            #xc='MGGA_X_SCAN+MGGA_C_SCAN',
            xc='TB09',
            #xc='GLLBSC',
            #####################################
            # TPSS and revTPSS:
            # File "/home/user/.miniconda3/envs/test-dft/lib/python3.11/site-packages/gpaw/xc/functional.py", line 119, in calculate_spherical
            # raise NotImplementedError
            #####################################
            #xc='TPSS',
            #xc='revTPSS',
            #####################################
            experimental={'magmoms': np.zeros((2, 3)), 'soc': True}, # new
            symmetry='off', # can't use symm with spinors?
            kpts={'size': (6, 6, 6), 'gamma': True},
            random=True,  # random guess (needed if many empty bands required)
            occupations=FermiDirac(0.001),
            txt='GS.log')
si.calc = calc
si.get_potential_energy()
ef = calc.get_fermi_level()

# Restart from ground state and fix potential:
calc = si.calc.fixed_density(
    txt='BANDS.log',
    kpts={'path': 'LGX', 'npoints': 60}
    )

bs = calc.band_structure()
bs = bs.subtract_reference()

bs.plot(show=False, emax=6, emin=-13)
plt.ylabel('E [eV]')
plt.ylim(-3, 3)
plt.grid()
plt.tight_layout()
plt.savefig('GaAs_MGGA_SOC_GPAW.png')
plt.show()