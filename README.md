# Testing DFT codes

Testing open-source DFT codes with MGGA or HSE functionals trying to get bands with correct gaps.

The following open-source do not currently implement MGGA+SOC: **Quantum Espresso**, **GPAW**. The open-source code **Octopus** has a branch *noncollinear_becke_roussel* where the noncollinear MGGA+SOC case is considered, but not yet merged. We'll test it here. We didn't try the open-source code **Exciting** yet. On the other side, the commercial code **VASP** works great with MGGA+spinors.

## Summary of results

- **VASP**
    - MGGA gives really good gaps and runs properly with SOC.
- **Quantum Espresso**
    - Meta-GGA works only for collinear case.
- **Octopus:**
    - Branch *noncollinear_becke_roussel*
    - Meta-GGA works only for collinear case.
- **GPAW:**
    - Meta-GGA works only for collinear case.
    - MGGA without SOC: gap still too small.
    - Often getting the error: `too many bands for non-collinear calculation`
- **Exciting:**
    - Not sure if we will try it
